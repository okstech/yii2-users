<?php

namespace oks\users;

use Yii;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface{
    const ALIAS = "@vendor/oks/yii2-users/src";
    public function bootstrap($app)
    {
        $this->registerTranslations();
    }
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['oks-user'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
            'basePath' => self::ALIAS.'/messages',
            'fileMap' => [
                'oks-user' => 'main.php',
            ],
        ];
    }
}