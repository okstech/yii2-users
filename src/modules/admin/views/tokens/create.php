<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model oks\users\models\Tokens */

$this->title = Yii::t('oks-users','Create Tokens');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oks-users','Tokens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tokens-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
