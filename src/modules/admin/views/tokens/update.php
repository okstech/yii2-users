<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model oks\users\models\Tokens */

$this->title = Yii::t('oks-users','Update Tokens');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oks-users','Tokens'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->token_id, 'url' => ['view', 'id' => $model->token_id]];
$this->params['breadcrumbs'][] = Yii::t('oks-users','Update');
?>
<div class="tokens-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
