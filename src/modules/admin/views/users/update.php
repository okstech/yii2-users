<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model oks\users\models\Users */

$this->title = Yii::t('oks-users','Update User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oks-users','Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->login, 'url' => ['view', 'id' => $model->user_id]];
$this->params['breadcrumbs'][] = Yii::t('oks-users','Update User');
?>
<div class="users-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
