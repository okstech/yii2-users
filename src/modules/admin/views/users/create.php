<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model oks\users\models\Users */

$this->title = Yii::t('oks-users','Create Users');
$this->params['breadcrumbs'][] = ['label' => Yii::t('oks-users','Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
