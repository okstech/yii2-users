<?php

namespace oks\users\modules\user;

use Yii;
/**
 * user module definition class
 */
class Module extends \yii\base\Module
{
    const ALIAS = "@vendor/oks/yii2-users/src";
    public $confirmLink = "";
    public $unConfirmLink = "";
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'oks\users\modules\user\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        // custom initialization code goes here
    }


}
